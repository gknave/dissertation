\contentsline {chapter}{List of Figures}{x}{chapter*.3}
\gdef \the@ipfilectr {@-1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Low-dimensional and simplified models in mechanics}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}The challenge of understanding fluid forces}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}The geometric structure of flows}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Research Overview}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Research topics not covered in this dissertation}{6}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Samara-inspired design of dispersed sensors through additive manufacturing}{6}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Wake effects on tandem airfoil gliders}{7}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Energy harvesting with flow-induced vibration}{8}{subsection.1.5.3}
\contentsline {chapter}{Bibliography}{9}{chapter*.7}
\gdef \the@ipfilectr {}
\gdef \the@ipfilectr {@-2}
\contentsline {chapter}{\numberline {2}Wake stiffness as a nonlinear spring}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction}{12}{section.2.1}
\contentsline {section}{\numberline {2.2}Existing force models}{15}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Linear Wake Stiffness, \citet {assi_role_2013}}{16}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Quasi-steady coefficients in the wake, \citet {blevins_forces_2005}}{18}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Wake Stiffness as a Nonlinear Spring}{20}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Resonance of nonlinear wake stiffness}{23}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Extended Model}{24}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Negative drag in the near wake}{25}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Critical spacing}{26}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Lift dependence on drag}{27}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Piecewise model}{30}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Smooth fluid force model}{31}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Comment on force models}{36}{subsection.2.4.6}
\contentsline {section}{\numberline {2.5}Full Nonlinear Wake Stiffness Model}{37}{section.2.5}
\contentsline {section}{\numberline {2.6}Example: tandem, tethered cylinders}{41}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Predictions from the model}{41}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Experimental Setup}{42}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Experimental Results}{43}{subsection.2.6.3}
\contentsline {subsubsection}{Amplitude of Oscillation}{43}{section*.22}
\contentsline {subsubsection}{Frequency Measurements and Wake Stiffness}{45}{section*.24}
\contentsline {subsection}{\numberline {2.6.4}The Gap Flow Switching Regime}{46}{subsection.2.6.4}
\contentsline {subsection}{\numberline {2.6.5}Experimental observations}{47}{subsection.2.6.5}
\contentsline {section}{\numberline {2.7}Discussion}{49}{section.2.7}
\contentsline {chapter}{Bibliography}{51}{chapter*.30}
\gdef \the@ipfilectr {}
\gdef \the@ipfilectr {@-3}
\contentsline {chapter}{\numberline {3}Global phase space structures in a model of passive descent}{56}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{57}{section.3.1}
\contentsline {section}{\numberline {3.2}A simple glider model}{59}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The terminal velocity manifold}{62}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Lift and drag as functional parameters}{63}{subsection.3.2.2}
\contentsline {subsubsection}{Falling flat plate}{64}{section*.35}
\contentsline {subsubsection}{The flying snake airfoil: the body shape of \textit {Chrysopelea paradisi}}{64}{section*.37}
\contentsline {subsubsection}{The NACA-0012 airfoil}{65}{section*.38}
\contentsline {subsection}{\numberline {3.2.3}Shape symmetry and force coefficients}{66}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Equilibrium points of the system}{67}{section.3.3}
\contentsline {section}{\numberline {3.4}Detecting the terminal velocity manifold}{70}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Stable manifold expansion}{71}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}The $v_z$-nullcline}{72}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Bisection method}{74}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Normal repulsion factor}{76}{subsection.3.4.4}
\contentsline {section}{\numberline {3.5}Pitch angle dependence of the terminal velocity manifold}{78}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}The terminal velocity manifold in extended phase space}{78}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Conceptualizing motion with the terminal velocity manifold}{80}{subsection.3.5.2}
\contentsline {subsubsection}{Gliding flight}{81}{section*.50}
\contentsline {subsubsection}{Fluttering plates}{82}{section*.52}
\contentsline {section}{\numberline {3.6}Summary and Conclusion}{83}{section.3.6}
\contentsline {chapter}{Bibliography}{85}{chapter*.55}
\gdef \the@ipfilectr {}
\gdef \the@ipfilectr {@-4}
\contentsline {chapter}{\numberline {4}The normal repulsion rate}{90}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{91}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Main result}{92}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Background and notation}{94}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Trajectory-normal repulsion rate}{94}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Trajectory-normal repulsion ratio}{95}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}The trajectory divergence rate}{96}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Trajectory divergence ratio}{99}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Physical interpretation of the trajectory divergence rate}{100}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Remarks on the trajectory divergence rate}{102}{subsection.4.3.3}
\contentsline {subsubsection}{(Lack of) objectivity of the trajectory divergence rate}{102}{section*.63}
\contentsline {subsubsection}{Relationship to Objective Eulerian Coherent Structures}{102}{section*.64}
\contentsline {section}{\numberline {4.4}Applications of the trajectory divergence rate}{103}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Approximation of slow manifolds and normally hyperbolic invariant manifolds}{104}{subsection.4.4.1}
\contentsline {subsubsection}{Search for the curve which minimizes the trajectory divergence rate}{105}{section*.67}
\contentsline {subsubsection}{Comparison with other methods}{107}{section*.70}
\contentsline {subsection}{\numberline {4.4.2}Approximation of hyperbolic Lagrangian coherent structures}{110}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Repulsion rate along a limit cycle}{111}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}Extension to higher dimensions}{113}{section.4.5}
\contentsline {section}{\numberline {4.6}Summary and conclusions}{117}{section.4.6}
\contentsline {chapter}{Bibliography}{118}{chapter*.78}
\contentsline {section}{\numberline {4.7}Derivation of Equation (\ref {eq:DivRate})}{122}{section.4.7}
\gdef \the@ipfilectr {}
\gdef \the@ipfilectr {@-5}
\contentsline {chapter}{\numberline {5}Summary and conclusions}{124}{chapter.5}
\contentsline {section}{\numberline {5.1}Future research directions}{125}{section.5.1}
\contentsline {chapter}{Bibliography}{127}{chapter*.79}
\gdef \the@ipfilectr {}
\contentsline {chapter}{Appendices}{129}{section*.80}
\gdef \the@ipfilectr {@-6}
\contentsline {chapter}{Appendix \numberline {A}Wake stiffness and its application: oscillating cylinders and flying snakes}{130}{Appendix.a.A}
\contentsline {section}{\numberline {A.1}Wake stiffness in the motion of tandem, tethered cylinders}{131}{section.a.A.1}
\contentsline {section}{\numberline {A.2}Applying wake stiffness to a model of flying snakes}{132}{section.a.A.2}
\contentsline {section}{\numberline {A.3}Conclusions}{133}{section.a.A.3}
\contentsline {chapter}{Appendix Bibliography}{134}{Appendix*.84}
\gdef \the@ipfilectr {}
