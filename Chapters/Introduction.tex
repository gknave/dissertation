\chapter{Introduction} \label{ch:introduction}
\section{Low-dimensional and simplified models in mechanics}
Mathematical models of complex phenomena provide frameworks to compare systems across different parameters. With modern flow visualization and computational techniques, we can know high resolution 3D information about fluid flows with more detail than ever before. However, understanding the implications of the detailed flow information is a challenge of its own. Simplified models, on the other hand, allow us to deeply understand the primary components of the physics of the system. Low-dimensional models can be visualized and understood in ways that high-dimensional models cannot. In the words of the statistician George Box \cite{box2005statistics},
\begin{quote}
	The most that can be expected from any model is that it can supply a useful approximation to reality: All models are wrong; some models are useful.
\end{quote}

There are many examples where simplified models play a unifying role in studying a complex phenomenon. When a moving body interacts with an incoming vortex wake, for example, the fluid forces it encounters may cause large oscillations. The parameter space for such a system is very large: streamwise and transverse body spacing, mass, damping, constraints of motion, and body shape. All of these factors make it very challenging to compare the variety of experimental and numerical investigations in the literature \cite{sarpkaya2004critical}. Comparing experimental and computational results with predictions from a model can supplement the comparison of raw experimental results and provide a more general understanding of the phenomena associated with flow-induced vibration.

As another example, animals which exhibit gliding behavior range from gliding ants to flying squirrels and flying snakes, each displaying a different body morphology, behavior during flight, and aerodynamic capabilities \cite{socha2015animals}. There are currently a few parameters which can be used to compare these models, such as wing loading, equilibrium glide angle and lift-to-drag ratio, but to compare their non-equilibrium glide behaviors, a new framework is necessary \cite{yeaton_global_2017}. With modern computational and experimental techniques, it is possible to visualize and simulate the fluid flows that move around these bodies \cite{wan2015computational,krishnan2014lift}, but interpreting the implications of differences in these results is challenging, particularly in relation to making predictions of future behavior. A simplified model which accounts for non-equilibrium trajectories would provide a useful framework to understand the differences in aerodynamic behavior across a variety of passively falling bodies.

Such simplified models can be better understood through tools which identify key features in their structure. Within the model of a system, there may be lower-dimensional geometric features which dominate the system's dynamics. For instance, in the model of gliding flight presented in Chapter \ref{ch:TVM}, there is a single 1-dimensional curve which attracts all possible trajectories of gliding bodies \cite{yeaton_global_2017,nave2018global}. By detecting and analyzing this attracting structure, we can more easily visualize the global dynamics of the model for comparison across multiple gliders. With these lower-dimensional features in flows, it is possible to gain a global understanding the key features in a flow, as they indicate the most influential regions of the flow.

In this dissertation, I present three works in manuscript format, each as a standalone work that has either been recently submitted or will be soon submitted to a journal for publication. The focus of this dissertation is to develop frameworks and tools which increase our understanding of systems by providing common points of comparison or useful visualizations. All three works fall within the realm of low dimensional models or tools, with a focus on forces resulting from fluid motion or the geometry of flows.

\section{The challenge of understanding fluid forces}
The problem of predicting the motion of flowing air or water, and therefore the resulting forces on a body within the flowing fluid, remains the focus of a broad array of research. Fluid motion is governed by the Navier-Stokes equations, for which even the existence and smoothness of solutions remains an open question. Only under specific assumptions do the Navier-Stokes equations allow for exact solutions. High-dimensional computational methods to simulate fluid motion through discrete approximations of the governing equations have led to many excellent results and improved our understanding of flows, but require significant computational resources and remain limited in application at present. Adding the coupling of body motion to the complexity of the problem, computational methods for fluid-structure interaction are only possible for a very narrow range of problems relative to the space of relevant problems \cite{dowell2001modeling}. Just as scientific pursuits continue in such high dimensional spaces, it is important to refine low-dimensional models of fluid motion and forcing.

In the face of the complexity of fluid forces on moving bodies, it is useful to consider more simplified models which give the dominant behavior. As the simplest example, the drag force on a fixed bluff body may be found to be, through the application of Bernoulli's equation or dimensional analysis,
\begin{equation}
F_D = C_D\frac{1}{2}\rho U_0^2A,
\end{equation}
where $A$ is the cross-sectional area of the body, $U_0$ is the free stream velocity of the fluid, $\rho$ is the fluid density, and the drag coefficient $C_D$ is a function of Reynolds number only \cite{batchelor2000introduction}. In the incompressible, inviscid limit, potential flow models have proven useful at predicting the dynamics resulting from flows, often using the Kirchhoff equations \cite{lamb1932hydrodynamics,leonard1997stability,shashikanth2002hamiltonian,borisov2006motion,kelly2010self}. In aerodynamics, methods such as the blade element theory, vortex panel methods, and lifting line theory all serve to provide an effective approximation for the fluid forces experienced during flight, although the practical forces experienced by an airplane may vary.

In this dissertation, I make use of the quasi-steady approach to modeling fluid forces, in which time dependent fluid forces are averaged to find the mean resultant fluid force at each static configuration. These forces, then, become dependent on a body's position, orientation, or configuration within a system. The dynamics are then allowed to evolve with the modeled fluid forces as functional inputs. Using the quasi-steady model for fluid forces allows us to investigate the dominant effect of fluid forces on the body without considering in detail the fluid dynamics. If we consider such models while acknowledging their shortcomings, they allow us to gain a deeper understanding of the dominant physics with the understanding that other components of the physics may dominate in other circumstances. Chapters \ref{ch:WakeStiffness} and \ref{ch:TVM} of this dissertation both focus on fluid force modeling, applying existing quasi-steady experimental results in two different contexts.

%Fluid-structure interaction and aeroelasticity are fields which often seek to subsume one another. Fluid-structure interaction textbooks frequently treat aeroelasticity as simply airfoil flutter and related phenomena - a subset of fluid-structure interaction. Meanwhile, aeroelasticity textbooks will treat fluid-structure interaction as simply vortex-induced oscillations of non-lifting bodies - a subset of aeroelasticity. This is a gross oversimplification, but both fields seek to be very general in their approach while still focusing on a few particular examples. In this dissertation, I seek to be more general in my definition of fluid-structure interaction, taking it to mean any situation in which the motions of a body and its surrounding fluid impact one another - the fluid motion resulting from the body's motion causes new forces on that body and the motion of the body results in a change of the fluid flow.

%Fluid-structure interaction is typically treated independently from  aerodynamics and models of flight. The biggest exception to this is the idea of aeroelasticity, which combines the effects of inertial forces, aerodynamic forces, and elastic forces. Different aeroelastic phenomena arise from different components of this triangle of forces, and 

\section{The geometric structure of flows}
When investigating the behavior of a mathematical model or fluid simulation or experiment, it is useful to look beyond the results of a single simulation or trajectory. While a trajectory gives the time-parametrized solution of a system of equations for a single initial condition $\mathbf{x}_0$,
\begin{equation}
\mathbf{F}_{\mathbf{x}_0}: t \mapsto \mathbf{x},
\end{equation}
the solution may be expressed in terms of the one-parameter, or time-$t$, flow map, which defines a mapping that takes any initial condition $\mathbf{x}_0$ to its position $\mathbf{x}$ at a fixed time $t$,
\begin{equation}
\mathbf{F}_t: \mathbf{x}_0 \mapsto \mathbf{x}.
\end{equation}
The space of vectors based at the initial points $\mathbf{v}_0(\mathbf{x}_0)$ is mapped forward along with the flow by the gradient of the time-$t$ flowmap,
\begin{equation}
\nabla\mathbf{F}_t: \mathbf{v}_0(\mathbf{x}_0) \mapsto \mathbf{v}(\mathbf{x}).
\end{equation}
From the flow map, its gradient, and the vector field itself, it is possible to look at structures which significantly influence the global behavior of the system. Points for which $F_t(\mathbf{x}_0)=\mathbf{x}_0$ for all time $t$, are \textit{equilibrium points}, which give the points at which the vector field goes to zero. These are the most basic geometric structures in a flow. The flow around an equilibrium point is organized by the \textit{stable, unstable, and center manifolds} of the equilibrium. For a saddle equilibrium point, which is associated with both stable and unstable manifolds, nearby trajectories are repelled from the stable manifold and attracted to the unstable manifold in forward time.

\textit{Invariant manifolds} have the property that any trajectory on the manifold must remain on the manifold for all time,
\begin{equation}
\Gamma = \{\mathbf{x}_0:\mathbf{F}_t({\mathbf{x}_0})\in\Gamma\,\, \forall t \}.
\end{equation}
In an autonomous system, every trajectory is itself an invariant manifold. Equilibrium points are 0-dimensional invariant manifolds, as the point trivially maps to itself. Other examples of invariant manifolds in flows include limit cycles, KAM tori \cite{wiggins2003introduction}, transition tubes \cite{koon2008dynamical,zhong2017tube}, and normally hyperbolic invariant manifolds \cite{wiggins2013normally}.

\textit{Coherent structures} are defined as material curves which act as barriers to transport in a flow. Within a fluid context, these are identified as Lagrangian if they are calculated from the trajectory advection (i.e., the time-$t$ flowmap) or Eulerian if they are calculated from the vector field itself. The regions of fluid separated by coherent structures are \textit{coherent sets}. In Chapters \ref{ch:TVM} and \ref{ch:RepRate}, I apply and develop methods to identify geometric structures such as the ones discussed here. Chapter \ref{ch:TVM} analyzes the geometric properties of a single mathematical model, focused on finding an attracting submanifold in phase space. Chapter \ref{ch:RepRate} introduces a diagnostic which can be used to approximate normally hyperbolic invariant manifolds and coherent structures or give local stability information of invariant manifolds.
%Both our understanding of fluids and our understanding of dynamic models benefit by understanding their flows, \textbf{for example}. Flows give a mapping from a given initial condition and time to a new position, representing the general solution to a differential equation. Because of the complexity and high dimensionality of a flow, it is useful to look at influential lower-dimensional geometric structures within those flows. Equilibrium pointsBy looking at the mapping of the flow, we can investigate these and more complex geometric structures.

%By focusing on a particular initial condition or time, we restrict the mapping to a particular case and gain more information. Trajectories give the flow for all time for a given initial condition,

%while the time-$T$ flowmap gives the mapping for all initial conditions to their final positions for a fixed time amount $T$,
%\begin{equation}
%\mathbf{F}_T: \mathbf{x}_0 \mapsto \mathbf{x}.
%\end{equation}
%The time-$T$ flowmap, then, gives the deformation of the entire space over a finite time $T$. While typical approaches to differential equations focus on the initial value problem, and therefore individual trajectories, the time-$T$ flowmap gives a global view of the geometry of the flow. %By looking at these mappings, we can look at the geometric structure of the flows.
%
%Three types of structures in particular give the key geometric features in a flow: elliptic, parabolic, and hyperbolic. Elliptic structures represent regions of high rotation such as a vortex, parabolic structures represent regions of high shear such as jets, and hyperbolic structures represent regions of high stretching. These structures, often referred to as coherent structures, give the ``skeleton'' of the flow and provide a simplified picture of how it behaves.
%
%\textbf{Finish this section!}
%Velocity field based methods for calculating elliptic structures include the Okubo-Weiss criterion \cite{okubo1970horizontal,weiss1991dynamics}, the Q criterion \cite{hunt1988eddies}, the $\Delta$ criterion \cite{chong1990general}, the $\lambda_2$ criterion \cite{jeong1995identification}, and the swirling strength criterion \cite{zhou1999mechanisms}.

%% Old version
%In the presence of advanced computational power, there remains a place for simplified models in our understanding of physical phenomena. With modern flow visualization and computational techniques, we can know high resolution 3D information about fluid flows with more detail than ever before. However, understanding the implications of the detailed flow information is a challenge. Simplified models allow us to deeply understand the primary components of the physics of the system. Lower-dimensional models can be visualized and understood in ways that high-dimensional models cannot. In the proposed work for this dissertation, I will consider a variety of low dimensional models of fluid forcing and methods to find low-dimensional structures in fluid flows.
%
%Low dimensional models that account for most of the physics can provide an incredibly useful role in understanding the phenomena they model, particularly when they are approached with the appropriate perspective. There are a variety of techniques that are available to us for two and three dimensions that are not possible in higher dimensions. Low dimensional models can be visualized more easily and viewed from a global perspective, analyzing all of the possible dynamics. These visualization techniques provide a deeper understanding of the system, giving intuition to those interested in applying concepts to design new systems. They allow a variety of possibilities to be computed rapidly, analyzing the differences between them. By stating assumptions and deeply understanding their implications, models give us tangible ways to understand physical phenomena. In the words of George Box \cite{box2005statistics},
%\begin{quote}
%	The most that can be expected from any model is that it can supply a useful approximation to reality: All models are wrong; some models are useful
%\end{quote}
%
%%Fluid motion both empowers and endangers the structures it interacts with. It has the ability to produce flight, generate power, and disperse plant seeds as well as the capacity to destroy bridges, destabilize underwater structures, and carry diseases. While the study of these interactions goes back centuries, recent developments in high speed photography, particle image velocimetry, and computational fluid dynamics have provided significant new insights into fluid behavior in the past few years.
%
%Within high-dimensional models themselves, we can also seek out lower-dimensional structure. Consider the equilibria of a dynamical system. These 0-dimensional structures organize the way we think about the flow: trajectories are drawn toward stable equilibria, circle around neutrally stable equilibria, or are both pushed and pulled by a saddle. Similarly, there may be 1- or 2-dimensional features which influence a flow. Coherent structures organize a flow by attracting, repelling, or encircling nearby trajectories, affecting how fluid particles are advected.


%\begin{itemize}
%	\item A variety of new technologies and techniques have emerged, particularly over the last twenty years, to understand the detailed motion of fluids around the bodies moving inside them.
%	\item However, understanding the feedback between fluid motion and body motion presents a more significant challenge. When it is possible, it takes significant computing resources.
%	\item Low dimensional models that account for most of the dynamics can provide an incredibly useful role in understanding the systems they model, particularly when they are approached with the appropriate perspective.
%	\item ``All models are wrong. Some are useful" - George Box
%	\item Deeper understanding of dominant physics
%	\item Generalizable to other systems
%	\item Provides strong intuition, visualization of phenomena
%	\item Can be used to design engineered systems, robotics, improved airdrops, power generation
%	\item We will consider three systems of fluid-structure interaction: flow-induced vibration of cylinders, the dynamics of gliding animals or 
%\end{itemize}


\section{Research Overview}
%\addcontentsline{toc}{subsection}{\protect\numberline{}Research Overview}
This dissertation comprises three projects, all of which related to the ideas of fluid forces on bodies and the geometric structure of flows.

In Chapter \ref{ch:WakeStiffness}, I consider a model for the forces that an incoming bluff body wake enacts on a moving body. The idea of linear wake stiffness is combined with an updated nonlinear model for quasi-steady fluid forces to develop the framework of nonlinear wake stiffness. This model is then applied in comparison with kinematic experiments on a new configuration of cylinders. This framework can be used to compare disparate experiments and simulate complex fluid-structure interactions across regimes of fluid forcing. We extend existing models in the literature to develop a framework for comparing different systems and experimentally analyze one such system using the new framework.

In Chapter \ref{ch:TVM}, I use a two degree-of-freedom model to understand the aerodynamic descent of gliding animals and fluttering seeds. Within the phase space of this 2-dimensional model, a 1-dimensional attracting manifold emerges. This attracting manifold serves as a higher-dimensional analog of terminal velocity --- the terminal velocity manifold. I show that nearly all of the dynamics of gliding flight and fluttering descent happen along this terminal velocity manifold within the 2-dimensional model and calculate the terminal velocity manifold in the 3-dimensional extended phase space.

In Chapter \ref{ch:RepRate}, I introduce a new method for detecting attracting or repelling structures in a flow, the trajectory divergence rate. This diagnostic may be used to rapidly approximate coherent structures in fluid flows or the flows of differential equations. I show applications of this method to find slow manifolds in slow-fast systems and how it compares to known methods in atmospheric fluid flows.

\section{Research topics not covered in this dissertation}
Because of the interdisciplinary nature of this research, and the opportunities that I have had to work with various faculty at Virginia Tech, my research interests vary wildly. There are several projects to which I have contributed which are beyond the scope of the present dissertation. Some of these projects represent extensions of the work presented in this thesis, but are beyond the primary scope of the work. A brief summary of some further work of my dissertation is presented here.

\subsection{Samara-inspired design of dispersed sensors through additive manufacturing}
As a part of an interdisciplinary team of biologists and engineers, I was involved in a project which took inspiration from autorotating plant seeds known as samaras or ``helicopter seeds.'' The shape of samaras allows them to spin rapidly as they fall from their parent tree, generating lift and slowing their descent. Therefore, they are more easily carried by the wind, which increases the area over which seeds may land and germinate and prevents the seedlings from having to grow in direct competition to the fully grown parent tree. From this natural inspiration, our team sought to develop engineered devices to mimic this behavior and carry loads such as sensors over a large area safely to the ground. As a part of this project, a team led by undergraduate students and mentored and assisted by graduate students and faculty was able to apply 3D printing techniques to recreate both silver maple seeds and Norway maple seeds which performed similarly to natural seeds. These natural and 3D printed seeds were compared by drop tests indoor, still-air conditions with high speed photography and in outdoor, windy conditions. Through another part of this project, a scaled up samara was developed which housed a pressure sensor that recorded data to an SD card throughout its descent. This device was tested from the Virginia Tech Smart Road bridge, successfully collectively data while surviving a descent of 175 feet without the assistance of a parachute.

\begin{figure}
	\centering
	\includegraphics[width=0.49\textwidth]{Figures/samara2}
	\includegraphics[width=0.49\textwidth]{Figures/samara1}
	\caption{Photos of experimental setup for real wind condition drops of 3D printed Norway maple seeds. (left) Seeds were dropped from a cherry picker at a 10ft height from the ground. (right) Close-up of 3D printed samara.}
	\label{fig:3d-printed-samaras}
\end{figure}

\subsection{Wake effects on tandem airfoil gliders}
For the 2016 International Congress of Theoretical and Applied Mechanics, I presented a talk which combined the ideas of nonlinear wake stiffness with the study of animal gliders \cite{nave_wake_2016}. Because the cross-sectional body shape of \textit{Chrysopelea paradisi} has been shown to be a bluff body at every angle of attack \cite{krishnan2014lift}, I hypothesized that the vortex wake shed by the upstream part of the body may enhance the lift forces experienced by the trailing bluff body. To give an approximation of the effects of this lift enhancement, I extended the tandem glider model of Jafari et al. \cite{jafari_theoretical_2014} to incorporate wake effects using the model of Blevins \cite{blevins_forces_2005} and simulated a collection of trajectories from various initial glide conditions. The extended abstract from this presentation is presented in Appendix \ref{ap:tandemGlider}.

\begin{figure}
	\centering
	\includegraphics[width=2.7in]{Figures/gliderNoWS}
	\includegraphics[width=2.7in]{Figures/gliderWS}
	\caption{Comparison of the results of simulated glides without(left) and with(right) a lift enhancement model based on wake stiffness. Green regions are considered ``stable'' glides based on whether the glider traveled at an angle shallower than $45^\circ$.}
	\label{fig:tandemGlider-results}
\end{figure}

Through this work, I was able to show that a lift enhancement that follows the form of the quasi-steady wake of circular cylinders enhances the stability of a tandem wing glider, given an offset wake centerline due to lift. Figure \ref{fig:tandemGlider-results} shows this stability enhancement by showing the initial conditions for which a glide is successful. Successful glides are those for which the glider travels more than 10m forward over a 10m vertical descent. The modeled wake effects of this project increase the range of initial conditions that allow for successful glides.

\subsection{Energy harvesting with flow-induced vibration}
The tethered cylinder arrangement considered in Chapter \ref{ch:WakeStiffness} was originally developed as a energy harvesting device. In the device, the trailing cylinder is rigidly attached to a vertical shaft allowed to rotate in the center of the upstream cylinder. The oscillating rotation of this shaft is then rectified to a single direction of rotation through the drivetrain pictured in Figure \ref{fig:enVIV}. This method for energy harvesting shows significant promise for energy harvesting at lower flow speeds where traditional hydrodynamic turbines fail.

\begin{figure}[b]
	\centering
	\includegraphics[height=1.8in]{Figures/enVIV-schema}
	\includegraphics[height=1.8in]{Figures/enVIV-gear-assembly}
	\caption{(left) Schematic of the vortex-induced vibration energy harvesting system, courtesy of Saikat Basu. (right) Photograph of custom gear assembly designed to rectify cylinder oscillations to single direction of rotation.}
	\label{fig:enVIV}
\end{figure}

\bibliographystyle{unsrtnat}
\bibliography{dissertation}