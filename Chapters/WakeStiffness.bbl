\begin{thebibliography}{47}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Bearman(1984)]{bearman_vortex_1984}
Peter~W. Bearman.
\newblock Vortex shedding from oscillating bluff bodies.
\newblock \emph{Annual Review of Fluid Mechanics}, 16\penalty0 (1):\penalty0
  195--222, 1984.

\bibitem[Zdravkovich(1977)]{zdravkovich_reviewreview_1977}
M.~M. Zdravkovich.
\newblock Review of flow interference between two circular cylinders in various
  arrangements.
\newblock \emph{Journal of Fluids Engineering}, 99\penalty0 (4):\penalty0
  618--633, December 1977.
\newblock ISSN 0098-2202.
\newblock \doi{10.1115/1.3448871}.

\bibitem[Zdravkovich(1988)]{zdravkovich_review_1988}
M.~M. Zdravkovich.
\newblock Review of interference-induced oscillations in flow past two parallel
  circular cylinders in various arrangements.
\newblock \emph{Journal of Wind Engineering and Industrial Aerodynamics},
  28\penalty0 (1–3):\penalty0 183--199, August 1988.
\newblock ISSN 0167-6105.
\newblock \doi{10.1016/0167-6105(88)90115-8}.

\bibitem[Sumner(2010)]{sumner_two_2010}
D.~Sumner.
\newblock Two circular cylinders in cross-flow: {A} review.
\newblock \emph{Journal of Fluids and Structures}, 26\penalty0 (6):\penalty0
  849--899, 2010.

\bibitem[Brika and Laneville(1993)]{brika_vortex-induced_1993}
D.~Brika and A.~Laneville.
\newblock Vortex-induced vibrations of a long flexible circular cylinder.
\newblock \emph{Journal of Fluid Mechanics}, 250:\penalty0 481--481, 1993.

\bibitem[Khalak and Williamson(1999)]{khalak_motions_1999}
A.~Khalak and C.~H.~K. Williamson.
\newblock Motions, forces and mode transitions in vortex-induced vibrations at
  low mass-damping.
\newblock \emph{Journal of Fluids and Structures}, 13\penalty0 (7):\penalty0
  813--851, 1999.

\bibitem[Williamson and Govardhan(2004)]{williamson_vortex-induced_2004}
C.H.K. Williamson and R.~Govardhan.
\newblock Vortex-{Induced} {Vibrations}.
\newblock \emph{Annual Review of Fluid Mechanics}, 36\penalty0 (1):\penalty0
  413--455, 2004.
\newblock \doi{10.1146/annurev.fluid.36.050802.122128}.

\bibitem[Assi et~al.(2010)Assi, Bearman, and Meneghini]{assi_wake-induced_2010}
G.~R.~S. Assi, P.~W. Bearman, and J.~R. Meneghini.
\newblock On the wake-induced vibration of tandem circular cylinders: the
  vortex interaction excitation mechanism.
\newblock \emph{Journal of Fluid Mechanics}, 661:\penalty0 365--401, 2010.
\newblock \doi{10.1017/S0022112010003095}.

\bibitem[Bearman(2011)]{bearman_circular_2011}
P.~W. Bearman.
\newblock Circular cylinder wakes and vortex-induced vibrations.
\newblock \emph{Journal of Fluids and Structures}, 27\penalty0
  (5–6):\penalty0 648--658, July 2011.
\newblock ISSN 0889-9746.
\newblock \doi{10.1016/j.jfluidstructs.2011.03.021}.

\bibitem[Bokaian and Geoola(1984)]{bokaian_wake-induced_1984}
A.~Bokaian and F.~Geoola.
\newblock Wake-induced galloping of two interfering circular cylinders.
\newblock \emph{Journal of Fluid Mechanics}, 146:\penalty0 383--415, September
  1984.
\newblock ISSN 1469-7645.
\newblock \doi{10.1017/S0022112084001920}.

\bibitem[Brika and Laneville(1999)]{brika_flow_1999}
D.~Brika and A.~Laneville.
\newblock The flow interaction between a stationary cylinder and a downstream
  flexible cylinder.
\newblock \emph{Journal of Fluids and Structures}, 13\penalty0 (5):\penalty0
  579--606, July 1999.
\newblock ISSN 0889-9746.
\newblock \doi{10.1006/jfls.1999.0220}.

\bibitem[Hover and Triantafyllou(2001)]{hover_galloping_2001}
F.~S. Hover and M.~S. Triantafyllou.
\newblock Galloping response of a cylinder with upstream wake interference.
\newblock \emph{Journal of Fluids and Structures}, 15\penalty0 (3):\penalty0
  503--512, 2001.

\bibitem[Assi et~al.(2013)Assi, Bearman, Carmo, Meneghini, Sherwin, and
  Willden]{assi_role_2013}
G.~R.~S. Assi, P.~W. Bearman, B.~S. Carmo, J.~R. Meneghini, S.~J. Sherwin, and
  R.~H.~J. Willden.
\newblock The role of wake stiffness on the wake-induced vibration of the
  downstream cylinder of a tandem pair.
\newblock \emph{Journal of Fluid Mechanics}, 718:\penalty0 210--245, 2013.

\bibitem[Zdravkovich(1974)]{zdravkovich_flow-induced_1974}
M.~M. Zdravkovich.
\newblock Flow-{Induced} {Vibrations} of {Two} {Cylinders} in {Tandem}, and
  their {Suppression}.
\newblock In Eduard Naudascher, editor, \emph{Flow-{Induced} {Structural}
  {Vibrations}}, pages 631--639, Karlsruhe, 1974. Springer-Verlag, Berlin,
  Germany.

\bibitem[Igarashi(1981)]{igarashi1981characteristics}
Tamotsu Igarashi.
\newblock Characteristics of the flow around two circular cylinders arranged in
  tandem: 1st report.
\newblock \emph{Bulletin of JSME}, 24\penalty0 (188):\penalty0 323--331, 1981.

\bibitem[Mizushima and Suehiro(2005)]{mizushima2005instability}
Jiro Mizushima and Norihisa Suehiro.
\newblock Instability and transition of flow past two tandem circular
  cylinders.
\newblock \emph{Physics of Fluids}, 17\penalty0 (10):\penalty0 104107, 2005.

\bibitem[Yang and Stremler(2018)]{yang2018critical}
Wenchao Yang and Mark~A. Stremler.
\newblock Critical spacing of stationary tandem circular cylinders at {$Re
  \approx 100$}.
\newblock \emph{In preperation}, 2018.

\bibitem[Carmo et~al.(2010)Carmo, Meneghini, and Sherwin]{carmo2010possible}
Bruno~Souza Carmo, Julio~Romano Meneghini, and SJ~Sherwin.
\newblock Possible states in the flow around two circular cylinders in tandem
  with separations in the vicinity of the drag inversion spacing.
\newblock \emph{Physics of Fluids}, 22\penalty0 (5):\penalty0 054101, 2010.

\bibitem[Tasaka et~al.(2006)Tasaka, Kon, Schouveiler, and
  Le~Gal]{tasaka2006hysteretic}
Yuji Tasaka, Seiji Kon, Lionel Schouveiler, and Patrice Le~Gal.
\newblock Hysteretic mode exchange in the wake of two circular cylinders in
  tandem.
\newblock \emph{Physics of Fluids}, 18\penalty0 (8):\penalty0 084104, 2006.

\bibitem[Huhe-Aode et~al.(1985)]{huhe1985visual}
Tatsuno Huhe-Aode et~al.
\newblock Visual studies of wake structure behind two cylinders in tandem
  arrangement.
\newblock \emph{Reports of Research Institute for Applied Mechanics, Kyushu
  University}, 32\penalty0 (99):\penalty0 1--20, 1985.

\bibitem[Papaioannou et~al.(2006)Papaioannou, Yue, Triantafyllou, and
  Karniadakis]{papaioannou2006three}
Georgios~V Papaioannou, Dick~KP Yue, Michael~S Triantafyllou, and George~E
  Karniadakis.
\newblock Three-dimensionality effects in flow around two tandem cylinders.
\newblock \emph{Journal of Fluid Mechanics}, 558:\penalty0 387--413, 2006.

\bibitem[Tong et~al.(2015)Tong, Cheng, and Zhao]{tong2015numerical}
Feifei Tong, Liang Cheng, and Ming Zhao.
\newblock Numerical simulations of steady flow past two cylinders in staggered
  arrangements.
\newblock \emph{Journal of Fluid Mechanics}, 765:\penalty0 114--149, 2015.

\bibitem[Ishigai et~al.(1972)Ishigai, Nishikawa, Nishimura, and
  Cho]{ishigai1972experimental}
Seikan Ishigai, Eiichi Nishikawa, Keiya Nishimura, and Katsuzo Cho.
\newblock Experimental study on structure of gas flow in tube banks with tube
  axes normal to flow: Part 1, karman vortex flow from two tubes at various
  spacings.
\newblock \emph{Bulletin of JSME}, 15\penalty0 (86):\penalty0 949--956, 1972.

\bibitem[Xu and Zhou(2004)]{xu2004strouhal}
G~Xu and Y~Zhou.
\newblock Strouhal numbers in the wake of two inline cylinders.
\newblock \emph{Experiments in Fluids}, 37\penalty0 (2):\penalty0 248--256,
  2004.

\bibitem[Kiya et~al.(1980)Kiya, Arie, Tamura, and Mori]{kiya1980vortex}
M~Kiya, M~Arie, H~Tamura, and H~Mori.
\newblock Vortex shedding from two circular cylinders in staggered arrangement.
\newblock \emph{Journal of Fluids Engineering}, 102\penalty0 (2):\penalty0
  166--171, 1980.

\bibitem[Ljungkrona et~al.(1991)Ljungkrona, Norberg, and
  Sunden]{ljungkrona1991free}
L~Ljungkrona, CH~Norberg, and B~Sunden.
\newblock Free-stream turbulence and tube spacing effects on surface pressure
  fluctuations for two tubes in an in-line arrangement.
\newblock \emph{Journal of Fluids and Structures}, 5\penalty0 (6):\penalty0
  701--727, 1991.

\bibitem[Kitagawa and Ohta(2008)]{kitagawa2008numerical}
T~Kitagawa and H~Ohta.
\newblock Numerical investigation on flow around circular cylinders in tandem
  arrangement at a subcritical reynolds number.
\newblock \emph{Journal of Fluids and Structures}, 24\penalty0 (5):\penalty0
  680--699, 2008.

\bibitem[Wang et~al.(2018)Wang, Alam, and Zhou]{wang2018two}
Longjun Wang, Md~Mahbub Alam, and Yu~Zhou.
\newblock Two tandem cylinders of different diameters in cross-flow: effect of
  an upstream cylinder on wake dynamics.
\newblock \emph{Journal of Fluid Mechanics}, 836:\penalty0 5--42, 2018.

\bibitem[Zdravkovich and Pridden(1977)]{zdravkovich1977interference}
MM~Zdravkovich and DL~Pridden.
\newblock Interference between two circular cylinders; series of unexpected
  discontinuities.
\newblock \emph{Journal of Wind Engineering and Industrial Aerodynamics},
  2\penalty0 (3):\penalty0 255--270, 1977.

\bibitem[Alam et~al.(2003)Alam, Moriya, Takai, and
  Sakamoto]{alam2003fluctuating}
Md~Mahbub Alam, Md~Moriya, K~Takai, and H~Sakamoto.
\newblock Fluctuating fluid forces acting on two circular cylinders in a tandem
  arrangement at a subcritical reynolds number.
\newblock \emph{Journal of Wind Engineering and Industrial Aerodynamics},
  91\penalty0 (1-2):\penalty0 139--154, 2003.

\bibitem[Biermann and Herrnstein~Jr(1933)]{biermann1933interference}
David Biermann and William~H Herrnstein~Jr.
\newblock The interference between struts in various combinations.
\newblock 1933.

\bibitem[Arie et~al.(1983)Arie, Kiya, Moriya, and Mori]{arie1983pressure}
M~Arie, M~Kiya, M~Moriya, and H~Mori.
\newblock Pressure fluctuations on the surface of two circular cylinders in
  tandem arrangement.
\newblock \emph{Journal of Fluids Engineering}, 105\penalty0 (2):\penalty0
  161--166, 1983.

\bibitem[Okajima(1979)]{okajima1979flows}
Atsushi Okajima.
\newblock Flows around two tandem circular cylinders at very high reynolds
  numbers.
\newblock \emph{Bulletin of JSME}, 22\penalty0 (166):\penalty0 504--511, 1979.

\bibitem[Fylling et~al.(2008)Fylling, Larsen, S{\o}dahl, Passano, Bech,
  Engseth, Lie, and Ormberg]{fylling2008riflex}
I~Fylling, C~Larsen, N~S{\o}dahl, E~Passano, A~Bech, A~Engseth, H~Lie, and
  H~Ormberg.
\newblock \emph{RIFLEX User's Manual 3.6}.
\newblock MARINTEK, Trondheim, Norway, 2008.

\bibitem[Orc(2018)]{orcaflex}
\emph{Orcaflex documentation, Online version 10.2c}.
\newblock Orcina Ltd., 2018.
\newblock URL
  \url{https://www.orcina.com/SoftwareProducts/OrcaFlex/Documentation/}.
\newblock Accessed: 2018-05-15.

\bibitem[Sarpkaya(2004)]{sarpkaya2004critical}
Turgut Sarpkaya.
\newblock A critical review of the intrinsic nature of vortex-induced
  vibrations.
\newblock \emph{Journal of {F}luids and {S}tructures}, 19\penalty0
  (4):\penalty0 389--447, 2004.

\bibitem[Blevins(2005)]{blevins_forces_2005}
Robert~D. Blevins.
\newblock Forces on and {Stability} of a {Cylinder} in a {Wake}.
\newblock \emph{Journal of Offshore Mechanics and Arctic Engineering},
  127\penalty0 (1):\penalty0 39--45, March 2005.
\newblock ISSN 0892-7219.
\newblock \doi{10.1115/1.1854697}.

\bibitem[Price(1975)]{price_wake_1975}
S.~J. Price.
\newblock Wake induced flutter of power transmission conductors.
\newblock \emph{Journal of Sound and Vibration}, 38\penalty0 (1):\penalty0
  125--147, 1975.

\bibitem[Price and Paidoussis(1984)]{price_aerodynamic_1984}
S.~J. Price and M.~P. Paidoussis.
\newblock The aerodynamic forces acting on groups of two and three circular
  cylinders when subject to a cross-flow.
\newblock \emph{Journal of Wind Engineering and Industrial Aerodynamics},
  17\penalty0 (3):\penalty0 329--347, September 1984.
\newblock ISSN 0167-6105.
\newblock \doi{10.1016/0167-6105(84)90024-2}.

\bibitem[Huse et~al.(1993)]{huse1993interaction}
Erling Huse et~al.
\newblock Interaction in deep-sea riser arrays.
\newblock In \emph{Offshore Technology Conference}. Offshore Technology
  Conference, 1993.

\bibitem[Price(1976)]{price_origin_1976}
S.~J. Price.
\newblock The origin and nature of the lift force on the leeward of two bluff
  bodies.
\newblock \emph{Aeronautical Quarterly}, 27:\penalty0 154--168, 1976.

\bibitem[Simpson and Price(1974)]{simpson1974use}
A~Simpson and SJ~Price.
\newblock Use of damped and undamped quasi-static aerodynamic models in study
  of wake-induced flutter.
\newblock In \emph{{IEEE} Transactions on Power Apparatus and Systems},
  number~6, pages 1741--1741. IEEE-INST ELECTRICAL ELECTRONICS ENGINEERS INC
  345 E 47TH ST, NEW YORK, NY 10017-2394, 1974.

\bibitem[Morse and Williamson(2009)]{morse2009prediction}
TL~Morse and CHK Williamson.
\newblock Prediction of vortex-induced vibration response by employing
  controlled motion.
\newblock \emph{Journal of Fluid Mechanics}, 634:\penalty0 5--39, 2009.

\bibitem[Pa{\"\i}doussis et~al.(2010)Pa{\"\i}doussis, Price, and
  De~Langre]{paidoussis2010fluid}
Michael~P Pa{\"\i}doussis, Stuart~J Price, and Emmanuel De~Langre.
\newblock \emph{Fluid-structure interactions: cross-flow-induced
  instabilities}.
\newblock Cambridge University Press, 2010.

\bibitem[Simpson and Flower(1977)]{simpson1977improved}
A~Simpson and JW~Flower.
\newblock An improved mathematical model for the aerodynamic forces on tandem
  cylinders in motion with aeroelastic applications.
\newblock \emph{Journal of Sound and Vibration}, 51\penalty0 (2):\penalty0
  183--217, 1977.

\bibitem[Norberg(2003)]{norberg2003fluctuating}
Christoffer Norberg.
\newblock Fluctuating lift on a circular cylinder: review and new measurements.
\newblock \emph{Journal of Fluids and Structures}, 17\penalty0 (1):\penalty0
  57--96, 2003.

\bibitem[Nave~Jr. et~al.(2016)Nave~Jr., Stremler, and Ross]{nave_wake_2016}
Gary~K. Nave~Jr., Mark Stremler, and Shane~D. Ross.
\newblock Wake stiffness and its application: oscillating cylinders and flying
  snakes.
\newblock In \emph{Proceedings of the 24th International Congress on
  Theoretical and Applied Mechanics ({ICTAM})}, 2016.

\end{thebibliography}
